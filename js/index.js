const mainField = document.getElementById('main-field');
const boxes = document.getElementsByClassName('main-field__box');
const restartBtn = document.getElementById('restart-btn');
const clearBtn = document.getElementById('clear-record-btn');
const p1 = document.getElementById('player-X');
const p2 = document.getElementById('player-0');
let stepX = true;
let steps = 0;

window.onload = function (){
    renderResults();
}

const doStep = (e) => {
    if (e.target.innerHTML !== '')return;
    let player = checkStep();
    switchPlayer(e);
    setValue(e, player);
    findWinner(player);
    steps++;
 }

const clearStorage = () => {
    localStorage.clear();
    p1.innerHTML = "0";
    p2.innerHTML = "0";
};

mainField.addEventListener('click', doStep);
clearBtn.addEventListener('click', clearStorage);

const switchPlayer = (e) => {
    if (e.target.innerHTML === '' ){stepX = !stepX;}
}

const checkStep = () => {
  return stepX ? 'X' : '0';
};

const setValue = (e, p) => {
    e.target.innerHTML = p;
}

const findWinner = (p) => {
    let winnerCombinations = [
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [0,4,8],
        [2,4,6],
    ];
    for(let combination of winnerCombinations){
       if(combination.every(e => boxes[e].innerHTML === p)){
           changeWinnerCombColor(combination)
           if (p === 'X') p1.innerHTML = Number(p1.innerHTML) + 1 + '';
           if (p === '0') p2.innerHTML = Number(p2.innerHTML) + 1 + '';
           setResults();
           return p === 'X' ? 'First player won!' : 'Second player won!';
       }
    }
    if (steps === 8) return 'Draw';
}

const changeWinnerCombColor = (comb) => {
    for (const i of comb) {
        boxes[i].style.cssText='background:#99f2c8; border-color: #1f4037';
    }
}

restartBtn.addEventListener('click', ()=>{
   location.reload();
});

const getResults = () => {
    return localStorage.getItem('results') === null ? {player1: 0, player2: 0} : JSON.parse(localStorage.getItem('results'));
}

const renderResults = () => {
    let res_obj = getResults();
    p1.innerHTML = res_obj.player1;
    p2.innerHTML = res_obj.player2;
}

const setResults = () => {
    let player1 = Number(p1.innerHTML);
    let player2 = Number(p2.innerHTML);
    let result_obj = {player1, player2};
    localStorage.setItem('results', JSON.stringify(result_obj));
}